package main

import (
	"bytes"
	"context"
	"crypto/rand"
	"flag"
	"fmt"
	"io"
	"log"
	mrand "math/rand"

	"os"

	golog "github.com/ipfs/go-log"
	"github.com/lazywei/go-opencv/opencv"
	crypto "github.com/libp2p/go-libp2p-crypto"
	host "github.com/libp2p/go-libp2p-host"
	peer "github.com/libp2p/go-libp2p-peer"
	pstore "github.com/libp2p/go-libp2p-peerstore"
	swarm "github.com/libp2p/go-libp2p-swarm"
	bhost "github.com/libp2p/go-libp2p/p2p/host/basic"
	ma "github.com/multiformats/go-multiaddr"
	gologging "github.com/whyrusleeping/go-logging"
	msmux "github.com/whyrusleeping/go-smux-multistream"
	yamux "github.com/whyrusleeping/go-smux-yamux"
)

var (
	webCamera     = new(opencv.Capture)
	sendImage     = make(chan *bytes.Buffer)
	win           = new(opencv.Window)
	targetPeerID  *peer.ID
	myAddr        ma.Multiaddr
	ha            host.Host
	width, height int
)

// go run main.go -l 8008

// makeBasicHost creates a LibP2P host with a random peer ID listening on the
// given multiaddress.
func makeBasicHost(listenPort int, randseed int64) (host.Host, error) {

	// If the seed is zero, use real cryptographic randomness. Otherwise, use a
	// deterministic randomness source to make generated keys stay the same
	// across multiple runs
	var r io.Reader
	if randseed == 0 {
		r = rand.Reader
	} else {
		r = mrand.New(mrand.NewSource(randseed))
	}

	// Generate a key pair for this host. We will use it at least
	// to obtain a valid host ID.
	priv, pub, err := crypto.GenerateKeyPairWithReader(crypto.RSA, 2048, r)
	if err != nil {
		return nil, err
	}

	// Obtain Peer ID from public key
	pid, err := peer.IDFromPublicKey(pub)
	if err != nil {
		return nil, err
	}

	// Create a multiaddress
	addr, err := ma.NewMultiaddr(fmt.Sprintf("/ip4/127.0.0.1/tcp/%d", listenPort))

	if err != nil {
		return nil, err
	}

	// Create a peerstore
	ps := pstore.NewPeerstore()

	// If using secio, we add the keys to the peerstore
	// for this peer ID.
	ps.AddPrivKey(pid, priv)
	ps.AddPubKey(pid, pub)

	// Set up stream multiplexer
	tpt := msmux.NewBlankTransport()
	tpt.AddTransport("/yamux/1.0.0", yamux.DefaultTransport)

	// Create swarm (implements libP2P Network)
	swrm, err := swarm.NewSwarmWithProtector(
		context.Background(),
		[]ma.Multiaddr{addr},
		pid,
		ps,
		nil,
		tpt,
		nil,
	)
	if err != nil {
		return nil, err
	}

	netw := (*swarm.Network)(swrm)

	basicHost := bhost.New(netw)

	// Build host multiaddress
	hostAddr, _ := ma.NewMultiaddr(fmt.Sprintf("/ipfs/%s", basicHost.ID().Pretty()))

	// Now we can build a full multiaddress to reach this host
	// by encapsulating both addresses:
	myAddr = addr.Encapsulate(hostAddr)
	log.Printf("I am %s\n", myAddr)
	log.Printf("Now run \"%s -l %d -d %s \" on a different terminal\n", os.Args[0], listenPort+1, myAddr)

	return basicHost, nil
}

func P2PServer(done chan bool) {

	// LibP2P code uses golog to log messages. They log with different
	// string IDs (i.e. "swarm"). We can control the verbosity level for
	// all loggers with:
	golog.SetAllLoggers(gologging.INFO) // Change to DEBUG for extra info

	// Parse options from the command line
	listenF := flag.Int("l", 0, "wait for incoming connections")
	seed := flag.Int64("seed", 0, "set random seed for id generation")
	target := flag.String("d", "", "target peer to dial")
	camera := flag.Int("c", 0, "the id of the camera")
	flag.Parse()

	if *listenF == 0 {
		log.Fatal("Please provide a port to bind on with -l")
	}
	var err error
	// Make a host that listens on the given multiaddress
	ha, err = makeBasicHost(*listenF, *seed)
	if err != nil {
		log.Fatal(err)
	}

	// activate webCamera
	webCamera = opencv.NewCameraCapture(*camera) // autodetect
	if webCamera == nil {
		panic("Unable to open camera")
	}
	defer webCamera.Release()

	// get some data from camera
	width = int(webCamera.GetProperty(opencv.CV_CAP_PROP_FRAME_WIDTH))
	height = int(webCamera.GetProperty(opencv.CV_CAP_PROP_FRAME_HEIGHT))

	fmt.Println("Camera width : ", width)
	fmt.Println("Camera height : ", height)

	// Set a stream handler on host A. /echo/1.0.0 is
	// a user-defined protocol name.

	if len(*target) > 0 {
		peerid, targetAddr := targetToPeerID(*target)
		// We have a peer ID and a targetAddr so we add it to the peerstore
		// so LibP2P knows how to contact it
		ha.Peerstore().AddAddr(peerid, targetAddr, pstore.PermanentAddrTTL)
		s, err := ha.NewStream(context.Background(), peerid, CAMERA_SENDER)
		if err != nil {
			log.Fatalln(err)
		}
		s.Write([]byte(myAddr.String() + TAG_S))
		s.Close()
		targetPeerID = &peerid
	}

	streamHandlers()

	select {
	case <-done:
		fmt.Println("Quited P2P")
		return
	}
}

func targetToPeerID(target string) (peer.ID, ma.Multiaddr) {
	// The following code extracts target's the peer ID from the
	// given multiaddress
	ipfsaddr, err := ma.NewMultiaddr(target)
	if err != nil {
		log.Fatalln(err)
	}

	pid, err := ipfsaddr.ValueForProtocol(ma.P_IPFS)
	if err != nil {
		log.Fatalln(err)
	}

	peerid, err := peer.IDB58Decode(pid)
	if err != nil {
		log.Fatalln(err)
	}

	// Decapsulate the /ipfs/<peerID> part from the target
	// /ip4/<a.b.c.d>/ipfs/<peer> becomes /ip4/<a.b.c.d>
	targetPeerAddr, _ := ma.NewMultiaddr(
		fmt.Sprintf("/ipfs/%s", peer.IDB58Encode(peerid)))
	targetAddr := ipfsaddr.Decapsulate(targetPeerAddr)
	return peerid, targetAddr
}
