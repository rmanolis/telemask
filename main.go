package main

import (
	"log"
	"os"

	"github.com/therecipe/qt/widgets"
)

var appDone = make(chan bool)

func main() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)
	qApp = widgets.NewQApplication(len(os.Args), os.Args)
	go P2PServer(appDone)
	ChatUI(appDone)

	qApp.Exec()
}
