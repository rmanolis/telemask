package main

import (
	"fmt"

	"github.com/lazywei/go-opencv/opencv"
	"github.com/therecipe/qt/gui"
	"github.com/therecipe/qt/widgets"
)

var qApp *widgets.QApplication
var messageBoard *widgets.QTextEdit

const ENTER_PRESS = 16777220

func ChatUI(done chan bool) {

	messageBoard = widgets.NewQTextEdit(nil)
	messageBoard.SetText("")
	messageBoard.SetReadOnly(true)

	send := widgets.NewQPushButton2("Send", nil)
	send.SetAutoDefault(true)
	send.SetDefault(true)

	inputText := widgets.NewQLineEdit(nil)

	inputText.ConnectKeyReleaseEvent(func(e *gui.QKeyEvent) {
		if e.Key() == ENTER_PRESS {
			sendMessage(inputText.Text())
			messageBoard.Append("me > " + inputText.Text())
			inputText.SetText("")
		}
	})
	send.ConnectClicked(func(c bool) {
		sendMessage(inputText.Text())
		messageBoard.Append("me > " + inputText.Text())
		inputText.SetText("")
	})

	var layout = widgets.NewQGridLayout2()
	layout.AddWidget(messageBoard, 0, 0, 1)
	layout.AddWidget(inputText, 1, 0, 0)
	layout.AddWidget(send, 1, 1, 2)

	var window = widgets.NewQMainWindow(nil, 0)
	window.SetWindowTitle("Chat")

	var centralWidget = widgets.NewQWidget(window, 0)
	centralWidget.SetLayout(layout)
	centralWidget.SetTabOrder(inputText, messageBoard)
	window.SetCentralWidget(centralWidget)
	window.ConnectCloseEvent(func(qe *gui.QCloseEvent) {
		fmt.Println("Closing Window")
		done <- true
	})
	window.Show()

}

func WindowCV(done chan bool) {
	// a new OpenCV window
	win = opencv.NewWindow("Go-OpenCV camera feed for Gtk-GUI")
	defer win.Destroy()

	select {
	case <-done:
		fmt.Println("Quited Window CV")
		return
	}
}
