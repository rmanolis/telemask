package main

import (
	"bufio"
	"bytes"
	"context"
	"fmt"
	"image/png"
	"log"
	"os"
	"strings"
	"time"

	"github.com/lazywei/go-opencv/opencv"
	net "github.com/libp2p/go-libp2p-net"
	peer "github.com/libp2p/go-libp2p-peer"
	pstore "github.com/libp2p/go-libp2p-peerstore"
)

const (
	CAMERA_RECEIVER  = "/camera_receiver/1.0.0"
	CAMERA_SENDER    = "/camera_sender/1.0.0"
	MESSAGE_RECEIVER = "/message_receiver/1.0.0"
	//MESSAGE_SENDER   = "/message_sender/1.0.0"
	TAG_S = "\n"
	TAG_C = '\n'
)

func streamHandlers() {
	go processFrameAndUpdate(sendImage)

	ha.SetStreamHandler(CAMERA_SENDER, func(s net.Stream) {
		log.Println("Got a new stream!")
		defer s.Close()
		if targetPeerID == nil {
			targetPeerID = getPeerID(s)
			requestCameraFromClient(s, *targetPeerID)
		}
		sendFrames(s, *targetPeerID)
	})

	ha.SetStreamHandler(CAMERA_RECEIVER, func(s net.Stream) {
		log.Println("Getting image")
		defer s.Close()
		showImage(s)
	})

	ha.SetStreamHandler(MESSAGE_RECEIVER, func(s net.Stream) {
		log.Println("Getting message")
		defer s.Close()
		buf := bufio.NewReader(s)
		str, err := buf.ReadString(TAG_C)
		if err != nil {
			log.Fatalln(err)
		}
		fmt.Println(str)
		message := strings.Replace(str, TAG_S, "", 1)
		messageBoard.Append(targetPeerID.String() + message)

	})
}

func sendMessage(msg string) {
	log.Println("Request to reply with frames")
	send_msg, err := ha.NewStream(context.Background(), *targetPeerID, MESSAGE_RECEIVER)
	if err != nil {
		log.Fatalln(err)
	}
	send_msg.Write([]byte(msg + TAG_S))
	send_msg.Close()
}

func requestCameraFromClient(s net.Stream, peerid peer.ID) {
	log.Println("Request to reply with frames")
	request_camera, err := ha.NewStream(context.Background(), peerid, CAMERA_SENDER)
	if err != nil {
		log.Fatalln(err)
	}
	request_camera.Write([]byte(myAddr.String() + TAG_S))
	request_camera.Close()
}

func sendFrames(s net.Stream, peerid peer.ID) {
	log.Println("Starting sending video")
	go WindowCV(appDone)
	// We have a peer ID and a targetAddr so we add it to the peerstore
	// so LibP2P knows how to contact it
	for {
		buf := <-sendImage
		log.Println("Sending picture")

		ns, err := ha.NewStream(context.Background(), peerid, CAMERA_RECEIVER)
		if err != nil {
			fmt.Println("error : ", err)
		}
		ns.Write(buf.Bytes())
		buf.Reset()
		ns.Close()
	}

}

// doEcho reads a line of data a stream and writes it back
func showImage(s net.Stream) {
	original_image, err := png.Decode(s) // put quality to 80%
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	img := opencv.FromImage(original_image)
	//opencv.SaveImage(fmt.Sprint("./imgs/output_", i, ".jpg"), img, []int{opencv.CV_IMWRITE_JPEG_QUALITY})
	win.ShowImage(img)
	img.Release()
}

func getPeerID(s net.Stream) *peer.ID {
	buf := bufio.NewReader(s)
	str, err := buf.ReadString(TAG_C)
	if err != nil {
		log.Fatalln(err)
	}
	fmt.Println(str)
	target := strings.Replace(str, TAG_S, "", 1)
	peerid, targetAddr := targetToPeerID(target)
	// We have a peer ID and a targetAddr so we add it to the peerstore
	// so LibP2P knows how to contact it
	ha.Peerstore().AddAddr(peerid, targetAddr, pstore.PermanentAddrTTL)
	return &peerid
}

func applyFilters(img *opencv.IplImage) *opencv.IplImage {

	w := img.Width()
	h := img.Height()

	// Create the output image
	cedge := opencv.CreateImage(w, h, opencv.IPL_DEPTH_8U, 3)

	// Convert to grayscale
	gray := opencv.CreateImage(w, h, opencv.IPL_DEPTH_8U, 1)
	edge := opencv.CreateImage(w, h, opencv.IPL_DEPTH_8U, 1)
	defer gray.Release()
	defer edge.Release()

	opencv.CvtColor(img, gray, opencv.CV_BGR2GRAY)

	opencv.Smooth(gray, edge, opencv.CV_BLUR, 3, 3, 0, 0)
	opencv.Not(gray, edge)

	// Run the edge detector on grayscale
	opencv.Canny(gray, edge, float64(3), float64(3*3), 3)

	opencv.Zero(cedge)
	// copy edge points
	opencv.Copy(img, cedge, edge)
	return cedge
}

func processFrameAndUpdate(sendImage chan *bytes.Buffer) {
	i := 0
	for {

		if webCamera.GrabFrame() {
			IplImgFrame := webCamera.RetrieveFrame(1)
			if IplImgFrame != nil {
				fmt.Println("Preparing image ", i)
				// Have to make the image frame
				// smaller, so that it does not blow up the GUI application.
				IplImgFrame = opencv.Resize(IplImgFrame, 0, height, opencv.CV_INTER_LINEAR)
				filteredImage := applyFilters(IplImgFrame)
				//applyFilters(IplImgFrame)
				goImgFrame := filteredImage.ToImage()

				// and then convert to []byte
				// with the help of jpeg.Encode() function

				frameBuffer := new(bytes.Buffer)
				err := png.Encode(frameBuffer, goImgFrame)
				if err != nil {
					panic(err)
				}
				sendImage <- frameBuffer
				fmt.Println("Prepared image ", i)
				IplImgFrame.Release()
				filteredImage.Release()
				i++
				time.Sleep(1 * time.Second)

			}
		}

	}

}
