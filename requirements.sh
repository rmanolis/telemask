go get github.com/ipfs/go-log
go get github.com/lazywei/go-opencv/opencv
go get github.com/libp2p/go-libp2p-crypto
go get github.com/libp2p/go-libp2p-host
go get github.com/libp2p/go-libp2p-net
go get github.com/libp2p/go-libp2p-peer
go get github.com/libp2p/go-libp2p-peerstore
go get github.com/libp2p/go-libp2p-swarm
go get -d github.com/libp2p/go-libp2p
go get github.com/mattn/go-gtk/gtk
go get github.com/multiformats/go-multiaddr
go get github.com/whyrusleeping/go-logging
go get github.com/whyrusleeping/go-smux-multistream
go get github.com/whyrusleeping/go-smux-yamux
go get github.com/libp2p/go-libp2p-nat